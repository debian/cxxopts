cxxopts (3.2.1-1) unstable; urgency=medium

  * New upstream version 3.2.1
  * New maintainer (Closes: #1065748)
  * d/copyright:
    + Update copyright years
    + Add new maintainer to copyright
    + Add Upstream-Contact information
  * d/control:
    + Replace deprecated build-dep pkg-config with pkgconf
    + Add new maintainer to maintainer field
  * d/p/0001-install-pkgconfig-file-into-arch-indep-usr-share-pkg.patch:
    + Add Forwarded info to patch header

 -- Shriram Ravindranathan <s20n@ters.dev>  Sun, 17 Mar 2024 14:50:20 +0000

cxxopts (3.1.1-2) unstable; urgency=medium

  * QA upload.
  * Orphan the package.

 -- Boyuan Yang <byang@debian.org>  Sat, 09 Mar 2024 13:05:37 -0500

cxxopts (3.1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright: Refresh information.
  * debian/patches: Refresh patches.

 -- Boyuan Yang <byang@debian.org>  Thu, 16 Feb 2023 13:47:13 -0500

cxxopts (3.0.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches: Refresh patch to maintain arch-indep pkgconfig.

 -- Boyuan Yang <byang@debian.org>  Fri, 22 Oct 2021 16:14:40 -0400

cxxopts (2.2.1-3) unstable; urgency=medium

  * Convert header-only library package to arch:all.
    (Closes: #975713)
  * debian/patches/0001-CMakeLists: Add patch from upstream to
    make CMake config files architecture-independent.
  * debian/control:
    + Mark binary package as Multi-Arch: foreign.
    + Bump Standards-Version to 4.6.0.

 -- Boyuan Yang <byang@debian.org>  Wed, 15 Sep 2021 13:41:01 -0400

cxxopts (2.2.1-2) unstable; urgency=medium

  * Source-only upload for testing migration.

 -- Boyuan Yang <byang@debian.org>  Fri, 23 Oct 2020 11:28:06 -0400

cxxopts (2.2.1-1) unstable; urgency=medium

  * Initial upload. (Closes: #971509)

 -- Boyuan Yang <byang@debian.org>  Wed, 30 Sep 2020 17:07:50 -0400
